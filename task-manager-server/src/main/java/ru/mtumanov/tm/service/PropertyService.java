package ru.mtumanov.tm.service;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.service.ILoggerService;
import ru.mtumanov.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "7777";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "11839";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    public static final String SERVER_HOST = "server.host";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_KEY_DEFAULT = "f809a09df";

    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    public static final String SESSION_TIMEOUT_DEFAULT = "6000";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String DB_USER = "database.username";

    @NotNull
    public static final String DB_USER_DEFAULT = "tm_user";

    @NotNull
    public static final String DB_PASSWORD = "database.password";

    @NotNull
    public static final String DB_PASSWORD_DEFAULT = "tm_user";

    @NotNull
    private static final String DB_URL = "database.url";

    @NotNull
    private static final String DB_URL_DEFAULT = "jdbc:postgresql://localhost:5432/postgres?currentSchema=public";

    @NotNull
    public static final String DB_DRIVER = "database.driver";

    @NotNull
    public static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DB_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DB_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DB_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DB_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DB_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String DB_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String SECOND_LVL_CACHE_KEY = "database.second_lvl_cache";

    @NotNull
    private static final String SECOND_LVL_CACHE_KEY_DEFAULT = "true";

    @NotNull
    private static final String REGION_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DB_USE_QUERY_CACHE = "database.use_query_cache";

    @NotNull
    private static final String DB_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DB_PROVIDER_CONFIG = "database.config_file_path";

    @NotNull
    private static final String DB_USE_MINIMAL_PUTS = "database.use_min_puts";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    public PropertyService() {
        try {
            properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
        } catch (Exception e) {
            loggerService.error(e);
        }
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key))
            return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey))
            return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @Override
    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @Override
    public int getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getDatabaseUser() {
        return getStringValue(DB_USER, DB_USER_DEFAULT);
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return getStringValue(DB_PASSWORD, DB_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseUrl() {
        return getStringValue(DB_URL, DB_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER, DB_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DB_DIALECT_KEY, DB_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return getStringValue(DB_HBM2DDL_AUTO_KEY, DB_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DB_SHOW_SQL_KEY, DB_SHOW_SQL_DEFAULT);
    }

    @Override
    @NotNull
    public String getSecondLvlCache() {
        return getStringValue(SECOND_LVL_CACHE_KEY, SECOND_LVL_CACHE_KEY_DEFAULT);
    }

    @Override
    @NotNull
    public String getFactoryClass() {
        return getStringValue(REGION_FACTORY_CLASS, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getDatabaseQueryCache() {
        return getStringValue(DB_USE_QUERY_CACHE, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getDatabaseRegionPrefix() {
        return getStringValue(DB_REGION_PREFIX, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getDatabaseConfigFile() {
        return getStringValue(DB_PROVIDER_CONFIG, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getDatabaseMinimalPuts() {
        return getStringValue(DB_USE_MINIMAL_PUTS, EMPTY_VALUE);
    }

}
