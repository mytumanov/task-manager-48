package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.model.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.ExistLoginException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.repository.model.ProjectRepository;
import ru.mtumanov.tm.repository.model.TaskRepository;
import ru.mtumanov.tm.repository.model.UserRepository;
import ru.mtumanov.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    protected UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
        this.taskRepository = new TaskRepository(connectionService);
        this.projectRepository = new ProjectRepository(connectionService);
    }

    @Override
    @NotNull
    protected IUserRepository getRepository() {
        return new UserRepository(connectionService);
    }

    @Override
    @NotNull
    public User create(@NotNull final String login, @NotNull final String password) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        repository.add(user);
        return user;
    }

    @Override
    @NotNull
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        if (email.isEmpty())
            throw new EmailEmptyException();

        @NotNull final User user = create(login, password);
        user.setEmail(email);
        repository.update(user);
        return user;

    }

    @Override
    @NotNull
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final Role role) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        @NotNull final User user = create(login, password);
        user.setRole(role);
        repository.update(user);
        return user;

    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    @NotNull
    public User findByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        return repository.findByEmail(email);

    }

    @Override
    @NotNull
    public User findById(@NotNull final String id) throws AbstractException {
        if (id.isEmpty())
            throw new EmailEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @NotNull
    public User removeByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        repository.remove(user);
        return user;
    }

    @Override
    @NotNull
    public User removeByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        @NotNull final User user = repository.findByEmail(email);
        repository.remove(user);
        return user;
    }

    @Override
    @NotNull
    public User setPassword(@NotNull final String id, @NotNull final String password) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user = repository.findOneById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
        return user;
    }

    @Override
    @NotNull
    public User userUpdate(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        @NotNull final User user = repository.findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.update(user);
        return user;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty())
            return false;
        return repository.isLoginExist(login);

    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty())
            return false;
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        user.setLocked(false);
        repository.update(user);
    }

}
