package ru.mtumanov.tm.api.repository.dto;

import ru.mtumanov.tm.dto.model.ProjectDTO;

public interface IDtoProjectRepository extends IDtoUserOwnedRepository<ProjectDTO> {

}
