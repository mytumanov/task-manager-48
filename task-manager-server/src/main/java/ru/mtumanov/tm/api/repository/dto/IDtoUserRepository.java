package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.model.UserDTO;

public interface IDtoUserRepository extends IDtoRepository<UserDTO> {

    @NotNull
    UserDTO findByLogin(@NotNull String login);

    @NotNull
    UserDTO findByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
