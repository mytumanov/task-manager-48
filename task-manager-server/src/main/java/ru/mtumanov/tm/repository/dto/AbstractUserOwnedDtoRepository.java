package ru.mtumanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.dto.IDtoUserOwnedRepository;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractDtoRepository<M>
        implements IDtoUserOwnedRepository<M> {

    protected AbstractUserOwnedDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final M entity) throws AbstractException {
        if (entity.getUserId() == null || entity.getUserId().isEmpty())
            throw new UserIdEmptyException();
        super.add(entity);
    }

    @Override
    public @NotNull M update(@NotNull final M entity) throws AbstractException {
        if (entity.getUserId() == null || entity.getUserId().isEmpty())
            throw new UserIdEmptyException();
        return super.update(entity);
    }

}
