package ru.mtumanov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.migration.AbstractSchemeTest;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.TaskDtoRepository;
import ru.mtumanov.tm.repository.dto.UserDtoRepository;
import ru.mtumanov.tm.service.ConnectionService;
import ru.mtumanov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class TaskRepositoryTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private UserDTO USER1;

    @NotNull
    private static final List<ProjectDTO> projectList = new ArrayList<>();

    @NotNull
    private final List<TaskDTO> taskList = new ArrayList<>();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @BeforeClass
    public static void init() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();

            @NotNull final UserDTO user = new UserDTO();
            user.setFirstName("UserFirstName ");
            user.setLastName("UserLastName ");
            user.setMiddleName("UserMidName ");
            user.setEmail("user@dot.ru");
            user.setLogin("USER");
            user.setRole(Role.USUAL);
            user.setPasswordHash("123");
            userRepository.add(user);
            USER1 = user;
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final ProjectDTO project = new ProjectDTO();
                project.setName("ProjectDTO name: " + i);
                project.setDescription("ProjectDTO description: " + i);
                project.setUserId(user.getId());
                projectRepository.add(project);
                projectList.add(project);
                for (int j = 0; j < NUMBER_OF_ENTRIES; j++) {
                    @NotNull final TaskDTO task = new TaskDTO();
                    task.setName("TaskDTO name: " + j);
                    task.setDescription("TaskDTO description: " + j);
                    task.setUserId(user.getId());
                    task.setProjectId(project.getId());
                    taskRepository.add(task);
                    taskList.add(task);
                }
            }

            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clearRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear();
            projectRepository.clear();
            userRepository.clear();
            taskList.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test task name");
        task.setDescription("Test task description");
        task.setUserId(USER1.getId());
        taskList.add(task);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
            @NotNull final List<TaskDTO> actualtaskList = taskRepository.findAll();
            assertEquals(taskList.size(), actualtaskList.size());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testClear() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        assertNotEquals(0, taskRepository.getSize(USER1.getId()));
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
            assertEquals(0, taskRepository.getSize(USER1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        for (@NotNull final TaskDTO task : taskList) {
            assertTrue(taskRepository.existById(USER1.getId(), task.getId()));
        }
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final List<TaskDTO> actualtasksUser1 = taskRepository.findAll(USER1.getId());
        assertEquals(actualtasksUser1.size(), taskList.size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        for (@NotNull final TaskDTO task : taskList) {
            assertEquals(task, taskRepository.findOneById(USER1.getId(), task.getId()));
        }
    }

    @Test(expected = NoResultException.class)
    public void testExceptionFindOneById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        for (int i = 0; i < 10; i++) {
            taskRepository.findOneById(USER1.getId(), UUID.randomUUID().toString());
        }
    }

    @Test
    public void testGetSize() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        assertEquals(taskList.size(), taskRepository.getSize(USER1.getId()));
    }

    @Test
    public void testRemove() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : taskList) {
                assertTrue(taskRepository.existById(USER1.getId(), task.getId()));
                taskRepository.removeById(USER1.getId(), task.getId());
                assertFalse(taskRepository.existById(USER1.getId(), task.getId()));
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : taskList) {
                assertTrue(taskRepository.existById(task.getId()));
                taskRepository.removeById(task.getId());
                entityManager.clear();
                assertFalse(taskRepository.existById(task.getId()));
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testAindAllByProjectId() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository TaskDtoRepository = new TaskDtoRepository(entityManager);
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (final ProjectDTO project : projectList) {
            for (@NotNull final TaskDTO task : taskList) {
                if (task.getProjectId() == project.getId())
                    actualTasks.add(task);
            }
            List<TaskDTO> tasks = TaskDtoRepository.findAllByProjectId(USER1.getId(), project.getId());
            assertEquals(tasks, actualTasks);
            actualTasks.clear();
        }
    }

    @Test
    public void testExceptionFindAllBytaskId() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository TaskDtoRepository = new TaskDtoRepository(entityManager);
        assertEquals(0, TaskDtoRepository.findAllByProjectId("", projectList.get(0).getId()).size());
    }
}
