package ru.mtumanov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.dto.IDtoProjectTaskService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.migration.AbstractSchemeTest;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.TaskDtoRepository;
import ru.mtumanov.tm.service.dto.ProjectTaskDtoService;
import ru.mtumanov.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectTaskServiceTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static UserDTO USER1;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDtoUserService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IDtoProjectTaskService projectTaskService = new ProjectTaskDtoService(connectionService);

    @NotNull
    private static final List<TaskDTO> taskList = new ArrayList<>();

    @NotNull
    private static final ProjectDTO project = new ProjectDTO();

    @NotNull
    private static final ProjectDTO projectToBind = new ProjectDTO();

    @BeforeClass
    public static void init() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        USER1 = userService.create("UserFirstName 10", "password", Role.USUAL);

        try {
            entityManager.getTransaction().begin();
            projectToBind.setName("PROJECT To Bind");
            projectToBind.setDescription("Bind");
            projectToBind.setUserId(USER1.getId());
            projectRepository.add(projectToBind);

            project.setName("PROJECT");
            project.setDescription("PROject description");
            project.setUserId(USER1.getId());
            projectRepository.add(project);

            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final TaskDTO task = new TaskDTO();
                task.setName("TaskDTO binded name: " + i);
                task.setDescription("TaskDTO binded description: " + i);
                task.setUserId(USER1.getId());
                task.setProjectId(project.getId());
                taskRepository.add(task);
                taskList.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clearRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        userService.removeByLogin(USER1.getLogin());
        taskList.clear();
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);

        try {
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : taskList) {
                assertNotEquals(task.getProjectId(), projectToBind.getId());
                projectTaskService.bindTaskToProject(USER1.getId(), projectToBind.getId(), task.getId());
                @NotNull final TaskDTO actualTask = taskRepository.findOneById(task.getUserId(), task.getId());
                assertEquals(projectToBind.getId(), actualTask.getProjectId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testPrjEmptyBindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject(USER1.getId(), "", "123");
    }

    @Test(expected = IdEmptyException.class)
    public void testTskEmptyBindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject(USER1.getId(), "123", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBadUseryBindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject("123", project.getId(), "123");
    }

    @Test
    public void testRemoveProjectById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);

        assertNotEquals(0, taskRepository.findAllByProjectId(USER1.getId(), project.getId()).size());
        assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
        try {
            entityManager.getTransaction().begin();
            projectTaskService.removeProjectById(USER1.getId(), project.getId());
            entityManager.getTransaction().commit();
            assertFalse(projectRepository.existById(project.getUserId(), project.getId()));
            assertEquals(0, taskRepository.findAllByProjectId(USER1.getId(), project.getId()).size());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testProjectEmptyRemoveProjectById() throws Exception {
        projectTaskService.removeProjectById(USER1.getId(), "");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testProjectBadRemoveProjectById() throws Exception {
        projectTaskService.removeProjectById(USER1.getId(), "123");
    }

    @Test
    public void testUnbindTaskFromProject() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);

        try {
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : taskList) {
                assertEquals(project.getId(), task.getProjectId());
                projectTaskService.unbindTaskFromProject(USER1.getId(), project.getId(), task.getId());
                @NotNull final TaskDTO actualTask = taskRepository.findOneById(task.getUserId(), task.getId());
                assertNull(actualTask.getProjectId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testPrjEmptyUnbindTaskFromProject() throws Exception {
        projectTaskService.bindTaskToProject(USER1.getId(), "", "123");
    }

    @Test(expected = IdEmptyException.class)
    public void testTskEmptyUnbindTaskFromProject() throws Exception {
        projectTaskService.bindTaskToProject(USER1.getId(), "123", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBadUserUnbindTaskFromProject() throws Exception {
        projectTaskService.bindTaskToProject("123", project.getId(), "123");
    }
}
