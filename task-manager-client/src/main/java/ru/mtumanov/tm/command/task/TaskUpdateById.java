package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskUpdateByIdRq;
import ru.mtumanov.tm.dto.response.task.TaskUpdateByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskUpdateById extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRq request = new TaskUpdateByIdRq(getToken(), id, name, description);
        @NotNull final TaskUpdateByIdRs response = getTaskEndpoint().taskUpdateById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
