package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexRs extends AbstractProjectRs {

    public ProjectChangeStatusByIndexRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectChangeStatusByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}
