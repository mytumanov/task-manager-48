package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectShowByIdRs extends AbstractProjectRs {

    public ProjectShowByIdRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectShowByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}