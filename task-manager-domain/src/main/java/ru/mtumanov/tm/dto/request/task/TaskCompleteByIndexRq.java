package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIndexRq extends AbstractUserRq {

    @Nullable
    private Integer index;

    public TaskCompleteByIndexRq(@Nullable final String token, @Nullable final Integer index) {
        super(token);
        this.index = index;
    }

}
